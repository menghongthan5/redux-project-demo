import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './App.css';
import { fetchPost } from './redux/actions/postAction';

function App() {
  const dispatch = useDispatch()
  const state = useSelector(state => state.posts)

  useEffect(() => {
    dispatch(fetchPost())
  }, [])

  return (
    <div>
      <h1>All Post</h1>
      <ul>
        {
          state.map(post => <li key={post.id}>{post.title}</li>)
        }
      </ul>
    </div>
  );
}

export default App;
