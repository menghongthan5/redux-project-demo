import api from "../../utils/api"

export const fetchPost = () => async dp => {
    let response = await api.get('/posts');
    return dp({
        type: "FETCH_POST",
        payload: response.data
    })
}
