const initialState = {
    posts: [],
}

export const postReducer = (state = initialState, { type, payload }) => {
    switch (type) {
    case "FETCH_POST":
        return { ...state, posts: payload}

    default:
        return state
    }
}
