import axios from 'axios';

const post = axios.create({
    baseURL: 'https://jsonplaceholder.typicode.com',
});

export default api;
